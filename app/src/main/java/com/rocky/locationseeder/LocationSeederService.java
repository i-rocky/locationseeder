package com.rocky.locationseeder;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Icon;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;


public class LocationSeederService extends Service implements LocationListener {

    static final String ACTION_OPEN = "com.rocky.locationseeder.LocationSeederService.OPEN";
    static final String ACTION_START = "com.rocky.locationseeder.LocationSeederService.START_SEEDING";
    static final String ACTION_STOP = "com.rocky.locationseeder.LocationSeederService.STOP_SEEDING";
    static final String ACTION_SERVER_INFO = "com.rocky.locationseeder.LocationSeederService.SERVER_INFO";
    static final String ACTION_NOTIFICATION = "com.rocky.locationseeder.LocationSeederService.NOTIFICATION";

    static final String ACTION_BROADCAST = "com.rocky.locationseeder.LocationSeederService.BROADCAST";

    public static final int CONNECTiON_TIMEOUT_MILLISECONDS = 10000;

    private Handler h;
    private boolean started;
    private boolean notification;
    private int delay;
    private NotificationManager nm;
    private SharedPreferences sharedPref;
    Notification.Builder nBuilder;

    LocationManager locationManager;

    String secret;

    boolean SERVER_OK;

    @Override
    public void onCreate() {
        super.onCreate();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        showNotification();
        h = new Handler();
        delay = 15000;

        sharedPref = getApplicationContext().getSharedPreferences(LocationSeeder.KEY_PREFERENCES, Context.MODE_PRIVATE);

        secret = getSetting(LocationSeeder.KEY_SECRET);

        SERVER_OK = getSetting(LocationSeeder.KEY_SERVER_OK).equals("yes");

        if(getSetting(LocationSeeder.KEY_SEEDING).equals("yes")) {
            startSeeding();
        }

        notification = getSetting(LocationSeeder.KEY_NOTIFICATION).equals("yes");

        if(notification) {
            showNotification();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        if(intent==null)
            return Service.START_STICKY;
        String action = intent.getAction();
        switch (action) {
            case ACTION_START:
                startSeeding();
                break;
            case ACTION_STOP:
                stopSeeding();
                break;
            case ACTION_SERVER_INFO:
                secret = intent.getStringExtra(LocationSeeder.KEY_SECRET);
                break;
            case ACTION_NOTIFICATION:
                notification = intent.getBooleanExtra("show", false);
                saveSetting(LocationSeeder.KEY_NOTIFICATION, notification?"yes":"no");
                if(notification) {
                    showNotification();
                }
                else {
                    nm.cancel(R.string.notification_id);
                }
                break;
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        nm.cancel(R.string.notification_id);
        super.onDestroy();
    }

    /**
     * Start seeding location
     */
    private void startSeeding() {
        if(started)
            return;
        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(this, LocationSeeder.class);
            startActivity(intent);
            return;
        }
        started = true;
        updateNotification(getText(R.string.seeding_started));
        sendBroadcast("seed_switch", "start", null);
        saveSetting(LocationSeeder.KEY_SEEDING, "yes");
        h.post(notifyLocation);
    }

    /**
     * Stop seeding location
     */
    private void stopSeeding() {
        if(!started)
            return;
        started = false;
        updateNotification(getText(R.string.seeding_stopped));
        sendBroadcast("seed_switch", "stop", null);
        saveSetting(LocationSeeder.KEY_SEEDING, "no");
        h.removeCallbacks(notifyLocation);
    }

    /**
     * Initiate the notification
     */
    private void showNotification() {
        if(!notification)
            return;
        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent app = PendingIntent.getActivity(this, 0, new Intent(this, LocationSeeder.class), 0);
        Intent start = new Intent(getApplicationContext(), LocationSeederService.class);
        start.setAction(ACTION_START);
        start.setPackage(getPackageName());
        Intent stop = new Intent(getApplicationContext(), LocationSeederService.class);
        stop.setAction(ACTION_STOP);
        stop.setPackage(getPackageName());
        PendingIntent startService = PendingIntent.getService(this, 0, start, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent stopService = PendingIntent.getService(this, 0, stop, PendingIntent.FLAG_UPDATE_CURRENT);

        // Set the info for the views that show in the notification panel.
        nBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.shuttle_bus)
                .setTicker("Running")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(app)
                .addAction(new Notification.Action.Builder(Icon.createWithResource(this, R.drawable.start_sign), "Start", startService).build())
                .addAction(new Notification.Action.Builder(Icon.createWithResource(this, R.drawable.stop_sign), "Stop", stopService).build());

        CharSequence seeding_content = getText(started?R.string.seeding_started:R.string.seeding_stopped);
        updateNotification(seeding_content);
    }

    /**
     * Update notification
     *
     * @param content CharSequence
     */
    private void updateNotification(CharSequence content) {
        if(!notification)
            return;
        nBuilder
                .setContentText(content)
                .setContentTitle(getText(started?R.string.notification_title_active:R.string.notification_title_inactive));

        Notification notification = nBuilder.build();
        notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        // Send the notification.
        nm.notify(R.string.notification_id, notification);
    }

    /**
     * Notifier looper
     */
    private Runnable notifyLocation = new Runnable() {
        @Override
        public void run() {
            sendLocation();
            if(started)
                h.postDelayed(notifyLocation, delay);
        }
    };

    /**
     * Send location to server
     */
    private void sendLocation() {
        if(locationManager==null) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 10, this);
        }
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(getApplicationContext(), "GPS Provider disabled", Toast.LENGTH_LONG).show();
            return;
        }
        // get the last know location from your location manager.
        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            final Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location!=null) {
                sendBroadcast("location", "ok", location);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        uploadLocation(location);
                    }
                }).start();
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "GPS Permission not granted", Toast.LENGTH_LONG).show();
            h.removeCallbacks(notifyLocation);
            updateNotification("GPS permission not granted. Tap to open app.");
        }
    }

    private void uploadLocation(Location location) {
        if(secret.isEmpty()) {
            stopSeeding();
            sendBroadcast("server", "error", null);
            return;
        }
        if(!connected()) {
            sendBroadcast("message", "No Internet Connection", null);
            return;
        }
        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "Internet permission not granted", Toast.LENGTH_LONG).show();
            return;
        }
        try {
            String response = executeGetHttpRequest("https://thesummitshuttle.com/admin/?action=update-location&lat=" + location.getLatitude() + "&lng="+location.getLongitude() + "&secret=" + secret);
            if(response.equals("ok") && !SERVER_OK) {
                sendBroadcast("server", "ok", null);
                SERVER_OK = true;
                saveSetting(LocationSeeder.KEY_SERVER_OK, "yes");
            }
        }
        catch (MalformedURLException urlEx) {
            SERVER_OK = false;
            saveSetting(LocationSeeder.KEY_SERVER_OK, "no");
            stopSeeding();
        }
        catch (IOException ioEx) {
            //do nothing
        }
        catch (Exception ex) {
            SERVER_OK = false;
            saveSetting(LocationSeeder.KEY_SERVER_OK, "no");
            stopSeeding();
        }
    }

    private boolean connected() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

    }

    public static String executeGetHttpRequest(final String path) throws IOException {
        String result = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(path);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(CONNECTiON_TIMEOUT_MILLISECONDS);
            urlConnection.setReadTimeout(CONNECTiON_TIMEOUT_MILLISECONDS);
            result = readStream(urlConnection.getInputStream());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }

    private static String readStream(InputStream is) throws IOException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("US-ASCII")));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            total.append(line);
        }
        reader.close();
        return total.toString();
    }

    /**
     * Saves settings to SharedPreference
     *
     * @param key String
     * @param value String
     */
    private void saveSetting(String key, String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Get settings from SharedPreference
     *
     * @param key String
     * @return String
     */
    private String getSetting(String key) {
        return String.valueOf(sharedPref.getString(key, ""));
    }

    private void sendBroadcast(String action, String state, Location l) {
        Intent bIntent = new Intent(ACTION_BROADCAST);
        bIntent.putExtra("action", action);
        bIntent.putExtra("state", state);
        if(action.equals("location") && state.equals("ok")) {
            bIntent.putExtra("lat", l.getLatitude());
            bIntent.putExtra("lng", l.getLongitude());
            bIntent.putExtra("alt", l.getAltitude());
            bIntent.putExtra("acc", l.getAccuracy());
        }
        if(action.equals("server")) {
            bIntent.putExtra(LocationSeeder.KEY_SERVER_OK, state.equals("ok"));
            bIntent.putExtra("message", state);
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(bIntent);
    }




    @Override
    public void onLocationChanged(Location location) {}
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
    @Override
    public void onProviderEnabled(String provider) {}
    @Override
    public void onProviderDisabled(String provider) {}
}
