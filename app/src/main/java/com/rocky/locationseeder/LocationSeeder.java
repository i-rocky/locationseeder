package com.rocky.locationseeder;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class LocationSeeder extends AppCompatActivity {

    final static String SERVER_URL = "thesummitshuttle.com";
    final static String KEY_SECRET = "secret";
    final static String KEY_SEEDING = "seeding";
    final static String KEY_SERVER_OK = "server_ok";
    final static String KEY_NOTIFICATION = "notification";
    final static String KEY_PREFERENCES = "LocationSeederPreferences";
    final static int KEY_PERMISSION = 1;

    EditText server_url;
    EditText secret;
    Switch seed_switch;
    Switch notification_switch;
    TextView info;
    SharedPreferences sharedPref;
    ImageButton ask_permission;

    boolean PERMISSION_GRANTED;
    boolean SERVER_OK;

    String LOCATION_INFO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_location_seeder);

        sharedPref = LocationSeeder.this.getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);

        //setup server url field
        server_url = (EditText) findViewById(R.id.server_url);
        server_url.setText(SERVER_URL);
        server_url.setEnabled(false);

        //Setup secret field
        secret = (EditText) findViewById(R.id.secret);
        secret.setText(getSetting(KEY_SECRET));

        //setup seed switch
        seed_switch = (Switch) findViewById(R.id.seed_switch);
        setSeedChecked(getSetting(KEY_SEEDING).equals("yes"));

        //Setup notification switch
        notification_switch = (Switch) findViewById(R.id.notification_switch);
        notification_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                toggleNotification(isChecked);
            }
        });
        notification_switch.setChecked(getSetting(KEY_NOTIFICATION).equals("yes"));

        info = (TextView) findViewById(R.id.info);

        ask_permission = (ImageButton) findViewById(R.id.ask_permission);
        ask_permission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askPermission();
            }
        });

        //Start service
        Intent service = new Intent(this, LocationSeederService.class);
        service.setAction(LocationSeederService.ACTION_OPEN);
        startService(service);

        if(getSetting(KEY_SERVER_OK).equals("yes")) {
            //Send server information
            SERVER_OK = true;
            sendServerAndSecret();
        }
        else {
            SERVER_OK = false;
        }

        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            askPermission();
        }
        else {
            PERMISSION_GRANTED = true;
            ask_permission.setVisibility(View.INVISIBLE);
        }
        printInfo();
        secret.setEnabled(!seed_switch.isChecked());
    }

    private void setSeedChecked(boolean enabled) {
        seed_switch.setOnCheckedChangeListener(null);
        seed_switch.setChecked(enabled);
        seed_switch.setOnCheckedChangeListener(seedSwitchListener);
    }

    private CompoundButton.OnCheckedChangeListener seedSwitchListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                startSeeding();
            }
            else {
                stopSeeding();
            }
        }
    };

    private void askPermission() {
        ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, KEY_PERMISSION);
    }

    /**
     * Sends start seed command to service
     */
    private void startSeeding() {
        if(!sendServerAndSecret())
            return;
        Intent service = new Intent(LocationSeederService.ACTION_START);
        service.setPackage(this.getPackageName());
        startService(service);
        secret.setEnabled(false);
    }

    /**
     * Sends server information to service
     */
    private boolean sendServerAndSecret() {
        String secret_text = secret.getText().toString();
        if(secret_text.isEmpty()) {
            Toast.makeText(this, "Empty secret", Toast.LENGTH_LONG).show();
            return false;
        }

        saveSetting(KEY_SECRET, secret_text);

        Intent service = new Intent(LocationSeederService.ACTION_SERVER_INFO);
        service.setPackage(this.getPackageName());
        service.putExtra(KEY_SECRET, secret.getText().toString());
        startService(service);
        return true;
    }

    /**
     * Sends stop seeding command to service
     */
    private void stopSeeding() {
        Intent service = new Intent(LocationSeederService.ACTION_STOP);
        service.setPackage(this.getPackageName());
        startService(service);
        secret.setEnabled(true);
    }

    /**
     * Toggle notification
     *
     * @param show boolean
     */
    private void toggleNotification(boolean show) {
        Intent service = new Intent(LocationSeederService.ACTION_NOTIFICATION);
        service.setPackage(getPackageName());
        service.putExtra("show", show);
        startService(service);
    }

    /**
     * Saves settings to SharedPreference
     *
     * @param key String
     * @param value String
     */
    private void saveSetting(String key, String value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Get settings from SharedPreference
     *
     * @param key String
     * @return String
     */
    private String getSetting(String key) {
        return String.valueOf(sharedPref.getString(key, ""));
    }

    private BroadcastReceiver bReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            String state = intent.getStringExtra("state");
            switch (action) {
                case "seed_switch":
                    boolean status = state.equals("start");
                    setSeedChecked(status);
                    secret.setEnabled(!status);
                    break;
                case "location":
                    updateLocationInfo(intent);
                    break;
                case "server":
                    SERVER_OK = intent.getBooleanExtra(KEY_SERVER_OK, false);
                    break;
            }
            if (action.equals("message")) printInfo(state);
            else printInfo();
        }
    };

    private void updateLocationInfo(Intent intent) {
        LOCATION_INFO = "Last sent location";
        String state = intent.getStringExtra("state");
        if(state.equals("ok")) {
            LOCATION_INFO += "\nLatitude: " + intent.getDoubleExtra("lat", 0.0);
            LOCATION_INFO += "\nLongitude: " + intent.getDoubleExtra("lng", 0.0);
            LOCATION_INFO += "\nAltitude: " + intent.getDoubleExtra("alt", 0.0);
            LOCATION_INFO += "\nAccuracy: " + intent.getFloatExtra("acc", 0);
        }
        else
        {
            LOCATION_INFO = "Location: NULL";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(bReceiver, new IntentFilter(LocationSeederService.ACTION_BROADCAST));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case KEY_PERMISSION:
                PERMISSION_GRANTED = grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if(!PERMISSION_GRANTED)
                {
                    ask_permission.setVisibility(View.VISIBLE);
                }
                else
                {
                    ask_permission.setVisibility(View.INVISIBLE);
                }
        }
        setNotSeeding();
        printInfo();
    }

    private void setNotSeeding() {
        setSeedChecked(false);
        saveSetting(KEY_SEEDING, "no");
        secret.setEnabled(true);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(bReceiver);

        super.onPause();
    }

    private void printInfo() {
        printInfo("");
    }

    private void printInfo(String info_text) {
        String pre_info_text = "Permission: " + (PERMISSION_GRANTED ? "Granted" : "Denied");
        pre_info_text += "\nNotification: " + (notification_switch.isChecked() ? "Enabled" : "Disabled");
        pre_info_text += "\nSeeding: " + (seed_switch.isChecked() ? "Enabled" : "Disabled");
        pre_info_text += "\nServer Secret: " + (SERVER_OK ? "OK" : "ERROR");
        if(!info_text.equals(""))
            pre_info_text += "\n\n" + info_text;
        if(seed_switch.isChecked())
            pre_info_text += "\n\n" + (LOCATION_INFO != null ? LOCATION_INFO : "Waiting for next hit...");
        info.setText(pre_info_text);
    }
}
